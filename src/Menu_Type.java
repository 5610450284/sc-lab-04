
public class Menu_Type {

	public void type_One(int x){
		for (int i=1 ; i<=x ; i++){
			for (int j=1 ; j<=x ; j++){
				toString_Star();
			}
		}
	}
	
	public void type_Two(int x){
		for (int i=1 ; i<=x ;i++){
			for (int j=1 ; j<=i ;j++){
				toString_Star();
			}
		}
	}
	
	public void type_Three(int x){
		for (int i=1 ; i<=x ;i++){
			x = (x*2)-1;
			for (int j=1 ; j<=x ;j++){
				if (j%2 == 0){
					toString_Star();
				}
				else {
					toString_Underline();
				}
			}
		}
	}
	
	public void type_Four(int x){
		for (int i=1 ; i<=x ;i++){
			for (int j=1 ; j<=x ;j++){
				if ((i+j)%2 == 0){
					toString_Star();
				}
				else {
					toString_Space();
				}
			}
		}
	}
	
	public String toString_Star(){
		return "*";
	}
	public String toString_Underline(){
		return "_";
	}
	public String toString_Space(){
		return " ";
	}
}
