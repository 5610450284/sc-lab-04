import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class Interface {
	
	JFrame j;
	JPanel panel_left, panel_right;
	JLabel label_type, label_size, label_menu;
	JComboBox<String> cb_type, cb_size;
	JTextArea ta;
	JButton btn_run, btn_clear;
	
	public static void main(String[] args){
		new Interface();
	}
	
	public Interface(){
		j = new JFrame("SC - LAB - 04");
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setSize(600, 300);
		j.setLayout(null);
		
		String size[] = {"3x3" ,
						 "4x4" ,
						 "5x5"};
		
		String type[] = {"type 01" ,
						 "type 02" ,
						 "type 03" ,
						 "type 04"};
		
		panel_left = new JPanel();
		panel_left.setLayout(null);
		panel_left.setBounds(0, 0, 300, 300);
		
		panel_right = new JPanel();
		panel_right.setLayout(null);
		panel_right.setBounds(300, 0, 300, 300);
		
		label_menu = new JLabel("Menu");
		label_menu.setBounds(130, 20, 100, 20);
		
		label_type = new JLabel("Types");
		label_type.setBounds(60, 100, 100, 20);
		
		label_size = new JLabel("Sizes");
		label_size.setBounds(60, 140, 100, 20);
		
		cb_type = new JComboBox<String>(type);
		cb_type.setBounds(110, 100, 100, 20);
		
		cb_size = new JComboBox<String>(size);
		cb_size.setBounds(110, 140, 100, 20);
		
		btn_run = new JButton("Run");
		btn_run.setBounds(30, 200, 100, 30);
		
		btn_clear = new JButton("Clear");
		btn_clear.setBounds(160, 200, 100, 30);
		
		ta = new JTextArea();
		ta.setBounds(0, 0, 300, 300);
		
		j.add(panel_left);
		j.add(panel_right);
		
		
		panel_right.add(ta);
		
		
		panel_left.add(cb_size);
		panel_left.add(cb_type);
		panel_left.add(btn_run);
		panel_left.add(btn_clear);
		panel_left.add(label_size);
		panel_left.add(label_type);
		panel_left.add(label_menu);
		
		j.setResizable(false);
		j.setVisible(true);
	}
	
}
